# BluHaleBleManager

BluHaleBleManager is singleton java script class written for react-native to manage the connection with ble device BluHale. This is also used to write and read the data from BluHale device.

### reuirements:- 
To use BluHaleBleManager you have to install and set up following modules.
 - [react-native-ble-plx](https://github.com/dotintent/react-native-ble-plx/)

## How to use

Import BluHaleBleManager, initialise it with callback and call the function startScanner. It scans the nearby devices and trys to connect the device we want. See below Example:-

`
BluHaleBleManager.init(callback) 
`

`BluHaleBleManager.startScanner({ serialNumber, passKey, lastSyncTimestamp, prevConnectedDeviceId }) 
`
#### BluHaleBleManager.init(callback);
- (i) callback(err, data) is callback function. It retunrs the following information.
   - err :- If any error occurs while connecting the device.
   - data :- This includes information like 
     `DeviceName,
      DeviceId,
      SerialNumber,
      Battery,
      RTC,
      FirstDoseTimestamp,
      LastDoseTimestamp,
      DoseData,
      FirstLogEventTimestamp,
      LastLogEventTimestamp,
      LogEventsData,
      DataFetchCompleted,
   ,`

#### startScanner( { serialNumber, passKey, lastSyncTimestamp, prevConnectedDeviceId } )
- (i). serialNumber:= serial number of BluHale device.
- (ii) passKey:- passkey of BluHale device
- (iii) lastSyncTimestamp:- It will fetch data from given timestamp 
- (iv) prevConnectedDeviceId:- It is used for connection persistance without scanning the devices.

#### stopScanner () :-
-  This functon stops the scanning of ble devices.

    Example:-
   ` BluHaleBleManager.stopScanner()`



## Internal functions:-

To make successful connection with the device there are number of other function that are exposed in the BluHaleBleManager.

- #### connectDevice()
    This function stops the scanning of new devices. It establishes bluetooth connection between the mobile device and scanned BluHale ble device. Then discovers the services and characteristic of the connected device. Then it sends authentication command to the device. If authentication command succeeds then it sends "SetRTC" and "GetRTC" commands. The it sends next commands one by one in sequence i.e "GetBatteryLevel", "GetDosePeriod", "GetDoseData", "GetMissingDosePacket", "GetLogEventsPeriod", "GetLogEventData". After recording the response of these commands it closes the bluetooth connection with device.

- #### disconnectDevice()
    This function closes the bluetooth connection between mobile and connected ble device.

- #### bindDisconnectEvent()
    This function sets the listner to notify the disconnect event.

- #### discoverServicesAndCharacteristics()
    It discovers services and characteristics. It ignores generic services and only records custom services Then attaches listener/callback to discovered notifiable characteristics.

- #### writeCharacteristics(bluHaleCommand: string)
    It creates payload as per command received and then writes it to discovered writable characteristics of ble device.

## Internal commands for device:-
Following commands are used to fetch the respected infromation from the device.
  - Authenticate: '09'
  - SetRTC: '00'
  - GetRTC: '01'
  - GetBatteryLevel: '08'
  - GetDosePeriod: '05
  - GetDoseData: '06'
  - GetMissingDosePacket: '07'
  - GetLogEventsPeriod: '02'
  - GetLogEventData: '03'
  - GetMissingLogPacket: '04',
  - Error: 'fe'

  ## Flow Diagram   
  Besic idea about working     
![alt text](https://gitlab.com/gspuwar/blehaleblemanager/-/raw/main/Screenshot.png?inline=false)



## Architecture diagram

![alt text](https://gitlab.com/gspuwar/blehaleblemanager/-/raw/main/Arch.jpg?inline=false)

## 🔗 Links
Check below link to learn more about react-native-ble-plx

- [react-native-ble-plx](https://github.com/dotintent/react-native-ble-plx/)


